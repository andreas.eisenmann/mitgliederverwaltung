import { Message, Source, Member, Constraint } from "@/model"
import Vue from "vue"

const store = Vue.observable({
  sources: [] as Source[],
  messages: [] as Message[],
  members: {} as { [key: string]: Member[] },
  constraints: [] as Constraint[],
  loading: false
})

const getters = {
  getSources() {
    return store.sources
  },
  getConstraints() {
    return store.constraints
  },
  getMessages() {
    return store.messages
  },
  getMembers(): { [key: string]: Member[] } {
    return store.members
  },
  isLoading() {
    return store.loading
  }
}

const mutations = {
  addSource(source: Source) {
    store.sources.push(source)
  },
  addConstraint(constraint: Constraint) {
    store.constraints.push(constraint)
  },
  addMessages(messages: Message[]) {
    store.messages.push.apply(store.messages, messages)
  },
  clearMessages(source: Source) {
    store.messages = store.messages.filter(message => message.source != source)
  },
  clearConstraintMessages() {
    store.messages = store.messages.filter(message => !message.createdByConstraint)
  },
  addMember(member: Member, source: Source) {
    if (source.name in store.members) {
      let members = store.members[source.name]
      members.push(member)
      Vue.set(store.members, source.name, members)
    } else {
      Vue.set(store.members, source.name, [member])
    }
  },
  clearMembers(source: Source) {
    Vue.set(store.members, source.name, [])
  },
  setLoading(flag: boolean) {
    store.loading = flag
  },
}

export function getSources() {
  return getters.getSources()
}

export function getConstraints() {
  return getters.getConstraints()
}


export function addSource(source: Source) {
  mutations.addSource(source)
}

export function addConstraint(constraint: Constraint) {
  mutations.addConstraint(constraint)
}

export function getMessages() {
  return getters.getMessages()
}

export function addMessages(messages: Message[]) {
  mutations.addMessages(messages)
}

export function clearMessages(source: Source) {
  mutations.clearMessages(source)
}

export function clearConstraintMessages() {
  mutations.clearConstraintMessages()
}

export function getMembersBySource() {
  return getters.getMembers()
}

export function getMembers(sourceName: string) {
  let members = getters.getMembers()
  if (sourceName in members) return members[sourceName]
  return []
}

export function addMember(member: Member, source: Source) {
  mutations.addMember(member, source)
}

export function clearMembers(source: Source) {
  mutations.clearMembers(source)
}

export function isLoading() {
  return getters.isLoading()
}

export function setLoading(flag: boolean) {
  mutations.setLoading(flag)
}

