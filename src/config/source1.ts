import { Source, SourceConfiguration, Member } from "@/model"
import { parseDate, dateToString } from "@/utils"

const sourceName = "dfbnet"
const description = "Mitgliederdaten - CSV Export aus verein.dfbnet.org"
const expectedColumns = [
    "Nachname", "Vorname", "Straße", "PLZ", "Ort", "Land", "Geburtsdatum", "Telefon", "Mobil", "E-Mail",
    "Abteilung_1", "Abteilungsaustritt_1", // Hauptverein
    "Abteilung_2", "Abteilungsaustritt_2", // Triathlon
    "Abteilung_3", "Abteilungsaustritt_3", // Radsport
    "Abteilung_4", "Abteilungsaustritt_4", // Skilanglauf
    "Beitragsbezeichnung_1_2", "Beitragsende_1_2", // "Schüler/Studenten"
    "Beitragsbezeichnung_1_3", "Beitragsende_1_3", // Fördermitglieder
    "DTU-Startpass", "BRV-Lizenz", "Mitgliederliste", "Mitgliederliste Einschränkungen"]
const allowedValues = {
    "Beitragsbezeichnung_1_3": ["Fördermitglieder"],
    "Abteilung_1": ["Hauptverein"],
    "Abteilung_2": ["Triathlon"],
    "Abteilung_3": ["Radsport"],
    "Abteilung_4": ["Skilanglauf"],
    "Beitragsbezeichnung_1_2": ["ermäßigter Beitrag"],
    "Mitgliederliste": ["ja", "nein"],
    "Mitgliederliste Einschränkungen": [
        "Nachname", "Vorname", "Straße", "PLZ", "Ort", "Land", "Geburtsdatum", "Telefon", "Mobil", "E-Mail"
    ]
}

export class DFBNetMember implements Member {

    constructor(
        public lastname: string,
        public firstname: string,
        public street: string,
        public zip_code: string,
        public city: string,
        public country: string,
        public birthdate: Date,
        public phone: string,
        public mobile_phone: string,
        public email: string,
        public gender: string,

        public section1: string,
        public section1_exit: Date,
        public section2: string,
        public section2_exit: Date,
        public section3: string,
        public section3_exit: Date,
        public section4: string,
        public section4_exit: Date,
        public section5: string,
        public section5_exit: Date,
        public reduced_fee: boolean,
        public reduced_exit: Date,
        public triathlon_passport: boolean,
        public cycling_license: boolean,
        public memberlist: boolean,
        public memberlist_constraints: string) { }

    public isActive() {
        return this.section1_exit > new Date()
    }

    public paysReducedFee() {
        return this.reduced_fee && this.reduced_exit > new Date()
    }

    public isTriathlete() {
        return this.section2 != "" && this.section2_exit > new Date()
    }

    public isCyclist() {
        return this.section3 != "" && this.section3_exit > new Date()
    }

    public isSkier() {
        return this.section4 != "" && this.section4_exit > new Date()
    }

    public isSustainer() {
        return this.section5 != "" && this.section5_exit > new Date()
    }

    public birthdateAsString(): string {
        if (this.birthdate > new Date()) return ""
        return dateToString(this.birthdate)
    }

    public birthYear(): string {
        if (this.birthdate > new Date()) return ""
        return dateToString(this.birthdate, "YYYY")
    }
}

let modifierFunc = function (member: Member): Member {
    const dfbnetmember = <DFBNetMember>member
    var remove_columns: string[] = dfbnetmember.memberlist_constraints.split(",").map(value => value.trim())

    const lastname = (remove_columns.includes("Nachname")) ? "" : dfbnetmember.lastname
    const firstname = (remove_columns.includes("Vorname")) ? "" : dfbnetmember.firstname
    const street = (remove_columns.includes("Straße")) ? "" : dfbnetmember.street
    const zip_code = (remove_columns.includes("PLZ")) ? "" : dfbnetmember.zip_code
    const city = (remove_columns.includes("Ort")) ? "" : dfbnetmember.city
    const country = (remove_columns.includes("Land")) ? "" : dfbnetmember.country
    const birthdate = (remove_columns.includes("Geburtsdatum")) ? new Date('9999-12-31T00:00:00') : dfbnetmember.birthdate
    const phone = (remove_columns.includes("Telefon")) ? "" : dfbnetmember.phone
    const mobile_phone = (remove_columns.includes("Mobil")) ? "" : dfbnetmember.mobile_phone
    const email = (remove_columns.includes("E-Mail")) ? "" : dfbnetmember.email

    return new DFBNetMember(
        lastname,
        firstname,
        street,
        zip_code,
        city,
        country,
        birthdate,
        phone,
        mobile_phone,
        email,
        dfbnetmember.gender,
        dfbnetmember.section1,
        dfbnetmember.section1_exit,
        dfbnetmember.section2,
        dfbnetmember.section2_exit,
        dfbnetmember.section3,
        dfbnetmember.section3_exit,
        dfbnetmember.section4,
        dfbnetmember.section4_exit,
        dfbnetmember.section5,
        dfbnetmember.section5_exit,
        dfbnetmember.reduced_fee,
        dfbnetmember.reduced_exit,
        dfbnetmember.triathlon_passport,
        dfbnetmember.cycling_license,
        dfbnetmember.memberlist,
        dfbnetmember.memberlist_constraints)
}

let toMemberFunc = function (item: { [key: string]: string }): Member {
    let lastname = item["Nachname"]
    let firstname = item["Vorname"]
    let street = item["Straße"]
    let zip_code = item["PLZ"]
    let city = item["Ort"]
    let country = item["Land"]
    let birthdate = parseDate(item["Geburtsdatum"])
    let phone = item["Telefon"]
    let mobile_phone = item["Mobil"]
    let email = item["E-Mail"]
    let gender = item["Geschlecht"]
    let section1 = item["Abteilung_1"]
    let section1_exit = parseDate(item["Abteilungsaustritt_1"])
    let section2 = item["Abteilung_2"]
    let section2_exit = parseDate(item["Abteilungsaustritt_2"])
    let section3 = item["Abteilung_3"]
    let section3_exit = parseDate(item["Abteilungsaustritt_3"])
    let section4 = item["Abteilung_4"]
    let section4_exit = parseDate(item["Abteilungsaustritt_4"])
    let section5 = item["Beitragsbezeichnung_1_3"]
    let section5_exit = parseDate(item["Beitragsende_1_3"])
    let reduced_fee = item["Beitragsbezeichnung_1_2"] == "ermäßigter Beitrag" ? true : false
    let reduced_exit = parseDate(item["Beitragsende_1_2"])
    let triathlon_passport = item["DTU-Startpass"] == "ja" ? true : false
    let cycling_license = item["BRV-Lizenz"] == "ja" ? true : false
    let memberlist = item["Mitgliederliste"] == "ja" ? true : false
    let memberlist_constraints = item["Mitgliederliste Einschränkungen"]
    return new DFBNetMember(
        lastname,
        firstname,
        street,
        zip_code,
        city,
        country,
        birthdate,
        phone,
        mobile_phone,
        email,
        gender,
        section1,
        section1_exit,
        section2,
        section2_exit,
        section3,
        section3_exit,
        section4,
        section4_exit,
        section5,
        section5_exit,
        reduced_fee,
        reduced_exit,
        triathlon_passport,
        cycling_license,
        memberlist,
        memberlist_constraints)
}

let today = dateToString(new Date())
let membersTemplate1 = `
<h2 align="center">Mitgliederliste Karlsruher Lemminge - ${today}</h2>
<p align="left">In dieser Liste enthalten sind nur Mitglieder, die der Weitergabe ihrer persönlichen Daten 
zugestimmt haben. Die Zustimmung kann jederzeit schriftlich beim geschäftsführenden Vorstand widerrufen werden.</p>
<table style="border-spacing: 0" width=100%>
	<thead>
		<tr style="background: #CCC">
			<th align="left" valign="top" style="padding: 0 0.2em">Nachname</th>
            <th align="left" valign="top" style="padding: 0 0.2em">Vorname</th>
            <th align="left" valign="top" style="padding: 0 0.2em">Adresse</th>
            <th align="left" valign="top" style="padding: 0 0.2em">Geburtsdatum</th>
            <th align="left" valign="top" style="padding: 0 0.2em">Telefon</th>
            <th align="left" valign="top" style="padding: 0 0.2em">E-Mail</th>
            <th align="left" valign="top" style="padding: 0 0.2em">Triathlon</th>
            <th align="left" valign="top" style="padding: 0 0.2em">Radsport</th>
            <th align="left" valign="top" style="padding: 0 0.2em">Skilanglauf</th>
            <th align="left" valign="top" style="padding: 0 0.2em">Fördermitglied</th>
        </tr>
    </thead>
    <tbody>
        <tr v-for="(member, index) in members" v-bind:key="index" 
            v-bind:style="{ 'background': index % 2 === 1 ? '#CCC' : '' }">
            <td nowrap valign="top" style="padding: 0 0.2em">{{member.lastname}}</td>
            <td nowrap valign="top" style="padding: 0 0.2em">{{member.firstname}}</td>
            <td nowrap valign="top" style="padding: 0 0.2em">{{member.street}}<span v-if="member.street != ''"><br></span>{{member.zip_code}} {{member.city}}<span v-if="member.city != ''">, </span>{{member.country}}</td>
            <td nowrap valign="top" style="padding: 0 0.2em">{{member.birthdateAsString()}}</td>
            <td nowrap valign="top" style="padding: 0 0.2em">{{member.phone}}<span v-if="member.phone != ''"><br></span>{{member.mobile_phone}}</td>
            <td nowrap valign="top" style="padding: 0 0.2em">{{member.email}}</td>
            <td nowrap valign="top" align="center" style="padding: 0 0.2em"><span v-if="member.isTriathlete()">X</span></td>
            <td nowrap valign="top" align="center" style="padding: 0 0.2em"><span v-if="member.isCyclist()">X</span></td>
            <td nowrap valign="top" align="center" style="padding: 0 0.2em"><span v-if="member.isSkier()">X</span></td>
            <td nowrap valign="top" align="center" style="padding: 0 0.2em"><span v-if="member.isSustainer()">X</span></td>
        </tr>
    </tbody>
</table>
`

let membersTemplate2 = `<h2>Bestandsmeldung der Karlsruher Lemminge an den Badischen Sportbund - ${today}</h2>
<table style="border-spacing: 0">
	<thead>
		<tr style="background: #CCC">
			<th align="left" valign="top" style="padding: 0 0.2em">Name</th>
            <th align="left" valign="top" style="padding: 0 0.2em">Vorname</th>
            <th align="left" valign="top" style="padding: 0 0.2em"></th>
            <th align="left" valign="top" style="padding: 0 0.2em">Jahrgang</th>
            <th align="left" valign="top" style="padding: 0 0.2em">Geschlecht</th>
            <th align="left" valign="top" style="padding: 0 0.2em">Ski</th>
            <th align="left" valign="top" style="padding: 0 0.2em">Triathlon</th>
            <th align="left" valign="top" style="padding: 0 0.2em">Radsport</th>
        </tr>
    </thead>
    <tbody>
        <tr v-for="(member, index) in members" v-bind:key="index">
            <td nowrap valign="top" style="padding: 0 0.2em">{{member.lastname}}</td>
            <td nowrap valign="top" style="padding: 0 0.2em">{{member.firstname}}</td>
            <td nowrap valign="top" style="padding: 0 0.2em"></td>
            <td nowrap valign="top" style="padding: 0 0.2em">{{member.birthYear()}}</td>
            <td nowrap valign="top" style="padding: 0 0.2em">{{member.gender.charAt(0)}}</td>
            <td nowrap valign="top" align="center" style="padding: 0 0.2em"><span v-if="member.isSkier() || member.isSustainer()">X</span></td>
            <td nowrap valign="top" align="center" style="padding: 0 0.2em"><span v-if="member.isTriathlete()">X</span></td>
            <td nowrap valign="top" align="center" style="padding: 0 0.2em"><span v-if="member.isCyclist() || (!member.isSustainer() && !member.triathlon_passport)">X</span></td>
            
        </tr>
    </tbody>
</table>
`

const config = new SourceConfiguration(
    expectedColumns, allowedValues, toMemberFunc,
    {
        'Mitgliederliste': {
            templateString: membersTemplate1,
            filters: [(member) => member.isActive() && (<DFBNetMember>member).memberlist],
            modifiers: [modifierFunc]
        }
        ,
        'BSB Bestandsmeldung': {
            templateString: membersTemplate2,
            filters: [(member) => member.isActive()],
            modifiers: []
        }
    }
)
export const source = new Source(sourceName, description, config)
