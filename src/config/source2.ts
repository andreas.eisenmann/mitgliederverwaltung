import { Source, SourceConfiguration, Member } from "@/model"
import { dateToString } from "@/utils"

const name = "bwtv"
const description = "Triathlon-Verband Daten - CSV Export aus tabw.it4sport.de"
const expectedColumns: string[] = ["Name", "Vorname", "StartpassNr"]
const allowedValues = {}

export class BWTVMember implements Member {
    constructor(
        public lastname: string,
        public firstname: string,
        public passport_number: string) { }

    public isActive() {
        return true
    }
}

let toMemberFunc = function (item: { [key: string]: string }): Member {
    let lastname = item["Name"]
    let firstname = item["Vorname"]
    let passport_number = item["StartpassNr"]
    return new BWTVMember(lastname, firstname, passport_number)
}

let today = dateToString(new Date())
let membersTemplate = `
<h2 align="center">BWTV Startpassinhaber Karlsruher Lemminge - ${today}</h2>
<table style="border-spacing: 0" width=100%>
	<thead>
		<tr style="background: #CCC">
			<th align="left" valign="top" style="padding: 0 0.2em">Nachname</th>
            <th align="left" valign="top" style="padding: 0 0.2em">Vorname</th>
            <th align="left" valign="top" style="padding: 0 0.2em">Startpass Nummer</th>
        </tr>
    </thead>
    <tbody>
        <tr v-for="(member, index) in members" v-bind:key="index" 
            v-bind:style="{ 'background': index % 2 === 1 ? '#CCC' : '' }">
			<td nowrap valign="top" style="padding: 0 0.2em">{{member.lastname}}</td>
            <td nowrap valign="top" style="padding: 0 0.2em">{{member.firstname}}</td>
            <td nowrap valign="top" style="padding: 0 0.2em">{{member.passport_number}}</td>
        </tr>
    </tbody>
</table>
`

const config = new SourceConfiguration(
    expectedColumns, allowedValues, toMemberFunc,
    {
        'Mitgliederliste': {
            templateString: membersTemplate,
            filters: [],
            modifiers: []
        }
    })
export const source = new Source(name, description, config)