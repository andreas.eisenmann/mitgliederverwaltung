
import { Message, Severity, Constraint } from "@/model"
import { source as source1, DFBNetMember } from "./source1"
import { source as source2, BWTVMember } from "./source2"
import { source as source3, BRVMember } from "./source3"
import { getMembers } from "@/store"

const constraint1 = function (): Message[] {
    let messages: Message[] = []

    let members = getMembers(source1.name)
    for (var member of members) {
        let dfbnetmember = <DFBNetMember>member

        if (!dfbnetmember.isActive()) {
            if (dfbnetmember.triathlon_passport) {
                messages.push(new Message(
                    `Inactive member has still setting 'Startpass=ja'`,
                    `${dfbnetmember.firstname} ${dfbnetmember.lastname}`, Severity.Warning, source1, true))
            }
            if (dfbnetmember.cycling_license) {
                messages.push(new Message(
                    `Inactive member has still setting 'BRV Lizenz=ja'`,
                    `${dfbnetmember.firstname} ${dfbnetmember.lastname}`, Severity.Warning, source1, true))
            }
            if (dfbnetmember.memberlist) {
                messages.push(new Message(
                    `Inactive member has still setting 'Mitgliederliste=ja'`,
                    `${dfbnetmember.firstname} ${dfbnetmember.lastname}`, Severity.Warning, source1, true))
            }
        }
    }
    return messages
}

const constraint2 = function (): Message[] {
    let messages: Message[] = []

    let members = getMembers(source1.name)
    for (var member of members) {
        let dfbnetmember = <DFBNetMember>member
        if (!dfbnetmember.isActive()) continue

        let getMessagesForYear = function (year: number): Message[] {
            if (dfbnetmember.paysReducedFee() && (year - dfbnetmember.birthdate.getFullYear() > 26)) {
                return [new Message(
                    `Member pays reduced fee, but gets older than 26 in year ${year}`,
                    `${dfbnetmember.firstname} ${dfbnetmember.lastname}`, Severity.Warning, source1, true)]
            }
            return []
        }

        let messagesCurrentYear = getMessagesForYear(new Date().getFullYear())
        if (messagesCurrentYear.length > 0) {
            messages.push(...messagesCurrentYear)
            continue
        }
        let messagesNextYear = getMessagesForYear(new Date().getFullYear() + 1)
        messages.push(...messagesNextYear)
    }
    return messages
}

const constraint3 = function (): Message[] {
    let messages: Message[] = []
    let bwtvmembers = getMembers(source2.name)
    let brvmembers = getMembers(source3.name)

    for (var bwtvmember of bwtvmembers) {
        if (brvmembers.filter(
            brvmember => (
                brvmember.lastname === bwtvmember.lastname &&
                brvmember.firstname === bwtvmember.firstname))
            .length > 0) {
            messages.push(new Message(
                `Member is registered both at ${source2.name} and ${source3.name}`,
                `${bwtvmember.firstname} ${bwtvmember.lastname}`, Severity.Critical, source2, true))
        }
    }
    return messages
}

const constraint4 = function (): Message[] {
    let messages: Message[] = []
    let dfbnetmembers = getMembers(source1.name)
    let bwtvmembers = getMembers(source2.name)
    if (bwtvmembers.length == 0) return []

    for (var member of dfbnetmembers) {
        let dfbnetmember = <DFBNetMember>member
        if (!dfbnetmember.triathlon_passport) continue

        if (bwtvmembers.filter(
            bwtvmember => (
                bwtvmember.lastname === dfbnetmember.lastname &&
                bwtvmember.firstname === dfbnetmember.firstname))
            .length == 0) {
            messages.push(new Message(
                `Member has setting 'Triathlon Startpass=ja', but is not registered at ${source2.name}`,
                `${dfbnetmember.firstname} ${dfbnetmember.lastname}`, Severity.Critical, source1, true))
        }
    }
    for (var member of bwtvmembers) {
        let bwtvmember = <BWTVMember>member

        if (dfbnetmembers.length > 0 && dfbnetmembers.filter(
            dfbnetmember => (
                bwtvmember.lastname === dfbnetmember.lastname &&
                bwtvmember.firstname === dfbnetmember.firstname &&
                dfbnetmember.isActive()))
            .length == 0) {
            messages.push(new Message(
                `Member registered at ${source2.name}, but not an active member at ${source1.name}`,
                `${bwtvmember.firstname} ${bwtvmember.lastname}`, Severity.Critical, source2, true))
        }
    }
    return messages
}

const constraint5 = function (): Message[] {
    let messages: Message[] = []
    let dfbnetmembers = getMembers(source1.name)
    let brvmembers = getMembers(source3.name)
    if (brvmembers.length == 0) return []

    for (var member of dfbnetmembers) {
        let dfbnetmember = <DFBNetMember>member
        if (!dfbnetmember.isActive()) continue
        if (dfbnetmember.isSustainer()) continue
        if (dfbnetmember.triathlon_passport) continue

        if (brvmembers.filter(
            brvmember => (
                brvmember.lastname === dfbnetmember.lastname &&
                brvmember.firstname === dfbnetmember.firstname))
            .length == 0) {
            messages.push(new Message(
                `Member has setting 'Triathlon Startpass=nein', but is not registered at ${source3.name}`,
                `${dfbnetmember.firstname} ${dfbnetmember.lastname}`, Severity.Critical, source1, true))
        }
    }
    for (var member of brvmembers) {
        let brvmember = <BRVMember>member

        if (dfbnetmembers.length > 0 && dfbnetmembers.filter(
            dfbnetmember => (
                brvmember.lastname === dfbnetmember.lastname &&
                brvmember.firstname === dfbnetmember.firstname &&
                dfbnetmember.isActive()))
            .length == 0) {
            messages.push(new Message(
                `Member registered at ${source3.name}, but not an active member at ${source1.name}`,
                `${brvmember.firstname} ${brvmember.lastname}`, Severity.Critical, source3, true))
        }
    }
    return messages
}

const constraint6 = function (): Message[] {
    let messages: Message[] = []
    let dfbnetmembers = getMembers(source1.name)
    let bwtvmembers = getMembers(source2.name)
    let brvmembers = getMembers(source3.name)

    for (var member of dfbnetmembers) {
        let dfbnetmember = <DFBNetMember>member
        if (!dfbnetmember.isSustainer()) continue

        if (dfbnetmember.triathlon_passport) {
            messages.push(new Message(
                `Sustaining member has setting 'Startpass=ja'`,
                `${dfbnetmember.firstname} ${dfbnetmember.lastname}`, Severity.Warning, source1, true))
        }
        if (dfbnetmember.cycling_license) {
            messages.push(new Message(
                `Sustaining member has setting 'Radlizenz=ja'`,
                `${dfbnetmember.firstname} ${dfbnetmember.lastname}`, Severity.Warning, source1, true))
        }

        if (bwtvmembers.length > 0 && bwtvmembers.filter(
            bwtvmember => (
                bwtvmember.lastname === dfbnetmember.lastname &&
                bwtvmember.firstname === dfbnetmember.firstname))
            .length > 0) {
            messages.push(new Message(
                `Sustaining member registered at ${source2.name}`,
                `${dfbnetmember.firstname} ${dfbnetmember.lastname}`, Severity.Critical, source1, true))
        }

        if (brvmembers.length > 0 && brvmembers.filter(
            brvmember => (
                brvmember.lastname === dfbnetmember.lastname &&
                brvmember.firstname === dfbnetmember.firstname))
            .length > 0) {
            messages.push(new Message(
                `Sustaining member registered at ${source3.name}`,
                `${dfbnetmember.firstname} ${dfbnetmember.lastname}`, Severity.Critical, source1, true))
        }
    }

    return messages
}

const constraint7 = function (): Message[] {
    let messages: Message[] = []
    let dfbnetmembers = getMembers(source1.name)

    for (var member of dfbnetmembers) {
        let dfbnetmember = <DFBNetMember>member
        if (!dfbnetmember.isSustainer()) continue

        if (dfbnetmember.isTriathlete() || dfbnetmember.isCyclist() || dfbnetmember.isSkier()) {
            messages.push(new Message(
                `Sustaining member must not be assigned to a section`,
                `${dfbnetmember.firstname} ${dfbnetmember.lastname}`, Severity.Warning, source1, true))
        }
    }

    return messages
}

const constraint8 = function (): Message[] {
    let messages: Message[] = []
    let dfbnetmembers = getMembers(source1.name)

    for (var member of dfbnetmembers) {
        let dfbnetmember = <DFBNetMember>member
        if (!dfbnetmember.triathlon_passport) continue

        if (!dfbnetmember.isTriathlete()) {
            messages.push(new Message(
                `Member with triathlon passport must be assigned section 'Triathlon'`,
                `${dfbnetmember.firstname} ${dfbnetmember.lastname}`, Severity.Warning, source1, true))
        }
    }

    return messages
}

const constraint9 = function (): Message[] {
    let messages: Message[] = []
    let dfbnetmembers = getMembers(source1.name)

    for (var member of dfbnetmembers) {
        let dfbnetmember = <DFBNetMember>member
        if (!dfbnetmember.cycling_license) continue

        if (!dfbnetmember.isCyclist()) {
            messages.push(new Message(
                `Member with cycling license must be assigned section 'Cycling'`,
                `${dfbnetmember.firstname} ${dfbnetmember.lastname}`, Severity.Warning, source1, true))
        }
    }

    return messages
}

export const constraints: Constraint[] = [
    // inactive members must not have settings 'triathlon_passport', 'cycling_license', 'memberlist'
    constraint1,
    // show members with reduced fee that become older than 26 this or next year
    constraint2,
    // members must either be registered at bwtv or brv
    constraint3,
    // members with setting 'triathlon_passport' must be registered at source 'bwtv'
    // and vica versa: all people registered at 'bwtv' must be active members in 'dfbnet'
    constraint4,
    // members without setting 'triathlon_passport' must be registered at source 'brv'
    // and vica versa: all people registered at 'brv' must be active members in 'dfbnet'
    constraint5,
    // sustainers must not have settings 'triathlon_passport', 'cycling_license'
    // sustainers must not be registered at 'bwtv' nor 'brv'
    constraint6,
    // sustainers must not be part of a section
    constraint7,
    // triathlon passport owners must be part of section 'Triathlon'
    constraint8,
    // cycling license owners must be part of section 'Cycling'
    constraint9
    // TODO: if brv license or rtf wertungskarte then brvmember.status == active
    // TODO: if  brvmember.status == active, then brv license or rtf wertungskarte then
    // TODO: allow registration in bwtv and brv if status == active
]
