import { Source, SourceConfiguration, Member } from "@/model"
import { dateToString } from "@/utils"

const name = "brv"
const description = "Radsport-Verband Daten - CSV Export aus brv.it4sport.de"
const expectedColumns: string[] = ["Nachname", "Vorname", "Status"]
const allowedValues = {}

export class BRVMember implements Member {
    constructor(
        public lastname: string,
        public firstname: string,
        public status: string) { }

    public isActive() {
        return true
    }
}

let toMemberFunc = function (item: { [key: string]: string }): Member {
    let lastname = item["Nachname"]
    let firstname = item["Vorname"]
    let status = item["Status"]
    return new BRVMember(lastname, firstname, status)
}
let today = dateToString(new Date())
let membersTemplate = `
<h2 align="center">BRV Mitglieder Karlsruher Lemminge - ${today}</h2>
<table style="border-spacing: 0" width=100%>
	<thead>
		<tr style="background: #CCC">
			<th align="left" valign="top" style="padding: 0 0.2em">Nachname</th>
            <th align="left" valign="top" style="padding: 0 0.2em">Vorname</th>
            <th align="left" valign="top" style="padding: 0 0.2em">Status</th>
        </tr>
    </thead>
    <tbody>
        <tr v-for="(member, index) in members" v-bind:key="index" 
            v-bind:style="{ 'background': index % 2 === 1 ? '#CCC' : '' }">
			<td nowrap valign="top" style="padding: 0 0.2em">{{member.lastname}}</td>
            <td nowrap valign="top" style="padding: 0 0.2em">{{member.firstname}}</td>
            <td nowrap valign="top" style="padding: 0 0.2em">{{member.status}}</td>
        </tr>
    </tbody>
</table>
`

const config = new SourceConfiguration(
    expectedColumns, allowedValues, toMemberFunc,
    {
        'Mitgliederliste': {
            templateString: membersTemplate,
            filters: [],
            modifiers: []
        }
    }
)
export const source = new Source(name, description, config)