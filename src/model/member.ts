
export interface Member {
  lastname: string
  firstname: string
  isActive(): boolean;
}
