import { Source } from "@/model"

export enum Severity {
  Error = 'Error',
  Critical = 'Critical',
  Warning = 'Warning',
  OK = 'Ok'
}

export class Message {
  constructor(
    public subject: string,
    public details: string,
    public severity: Severity,
    public source: Source,
    public createdByConstraint: boolean = false) { }
}
