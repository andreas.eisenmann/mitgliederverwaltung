import { Source, Member, Message } from "@/model"

export interface Constraint {
    (): Message[]
}