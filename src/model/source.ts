import { Member } from "@/model"

export class Source {
  constructor(
    public name: string,
    public description: string,
    public configuration: SourceConfiguration) { }
}


export class SourceConfiguration {
  constructor(
    public expectedColumns: string[],
    public allowedValues: { [key: string]: Array<string> },
    public toMember: MaptoMemberFunc,
    public memberTemplates: MemberTemplates,
  ) { }

  public modify(member: Member, templateName: string): Member {
    for (var modifier of this.memberTemplates[templateName].modifiers) {
      member = modifier(member)
    }
    return member
  }

  public filter(member: Member, templateName: string): boolean {
    for (var filterFunc of this.memberTemplates[templateName].filters) {
      if (filterFunc(member)) {
        continue
      } else {
        return false
      }
    }
    return true
  }
}

type MemberTemplates = {
  [key: string]: {
    templateString: string,
    filters: FilterFunc[],
    modifiers: ModifierFunc[]
  }
}

export interface ModifierFunc {
  (member: Member): Member
}

export interface MaptoMemberFunc {
  (datarow: { [key: string]: string }): Member
}

export interface FilterFunc {
  (member: Member): boolean
}