import Papa, { ParseResult } from "papaparse"

import { Message, Source, Severity } from "@/model"


export class CSVReader {
  fileContent: string
  source: Source
  parseResult: ParseResult
  result = {} as any;

  private constructor(fileContent: string, source: Source) {
    this.fileContent = fileContent
    this.source = source
    this.result = {
      messages: [] as Message[],
      data: [] as any[]
    }

    // run the initial parse
    this.parseResult = Papa.parse(this.fileContent.trim(), { header: true })
  }

  private hasParseErrors(): boolean {
    for (var error of this.parseResult.errors) {
      var message = new Message("Cannot not parse CSV file", error.message, Severity.Error, this.source)
      this.result.messages = [message]
    }
    return this.parseResult.errors.length > 0
  }

  private hasMissingColumns(): boolean {
    // compare expected and actual columns
    var expectedColumns = this.source.configuration.expectedColumns
    var actualColumns = this.parseResult.meta.fields
    let missingColumns = expectedColumns.filter(
      column => !actualColumns.includes(column)
    )
    for (var column of missingColumns) {
      var message = new Message(
        "Expected column does not exist", `Missing column '${column}'`, Severity.Error, this.source)
      this.result.messages.push(message)
    }
    return missingColumns.length > 0
  }

  private hasInvalidValues(): boolean {
    let hasInvalidValues = false
    // compare actual against allowed values in columns
    var allowedValues = this.source.configuration.allowedValues

    this.parseResult.data.forEach((row, index) => {
      Object.entries<string>(row).forEach(entry => {
        var columnName = entry[0]
        if (!(columnName in allowedValues))
          return

        var columnValue = entry[1]
        var values = columnValue.split(",").map(value => value.trim())
        for (var value of values) {
          if (allowedValues[columnName].indexOf(value) === -1 && value != "") {
            hasInvalidValues = true
            var message = new Message(
              "Column contains invalid value",
              `Line ${index + 1}: Value '${value}' is not allowed in column '${columnName}'`,
              Severity.Error,
              this.source)
            this.result.messages.push(message)
          }
        }
      })
    })
    return hasInvalidValues
  }

  private run_checks() {
    // check and create error messages
    if (this.hasParseErrors()) return
    if (this.hasMissingColumns()) return
    if (this.hasInvalidValues()) return

    // all checks passed
    // create success message
    // var message = new Message(
    //   `Successfully fetched data for '${this.source.name}'`,
    //   `Found ${this.parseResult.data.length} data records`,
    //   Severity.OK,
    //   this.source)
    // this.result.messages = [message]
    this.result.data = this.parseResult.data
  }

  static read(fileContent: string, source: Source): any {
    var reader = new CSVReader(fileContent, source)
    reader.run_checks()
    return reader.result
  }

}