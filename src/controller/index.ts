import { map, flatMap, filter, tap, count } from 'rxjs/operators'

import { Source, Message, Severity, Member } from "@/model"
import {
  addMessages, addMember,
  clearMessages, clearConstraintMessages, clearMembers,
  setLoading, getConstraints
} from "@/store"

import { RXFileReader } from "./filereader"
import { CSVReader } from "./csvreader"


export function changeSourceData(file: File, source: Source) {

  setLoading(true)

  clearMessages(source)
  clearConstraintMessages()
  clearMembers(source)

  if (file == null) {
    setLoading(false)
    return
  }



  let readFile = (file: File) => new RXFileReader(file).read()
  let readData = (csv: string, source: Source): any => CSVReader.read(csv, source)
  let runConstraintChecks = () => {
    for (var constraint of getConstraints()) {
      addMessages(constraint())
    }
  }
  let addMemberCountMessage = (count: number) => {
    if (count > 0)
      addMessages([new Message(
        `Successful collected memberlist for '${source.name}'`,
        `Found ${count} members`, Severity.OK, source)])
  }

  // read the input CSV file to an RX Observable stream
  readFile(file)
    .pipe(
      // read the data from the CSV
      map(csv => readData(csv, source)),
      // report the messages from the read operation
      tap(result => addMessages(result.messages)),
      // flatten the data to single data rows
      map(result => result.data),
      flatMap(datarow => datarow),
      // convert a data row to a Member instance
      map(datarow => source.configuration.toMember(datarow as { [key: string]: string })),
      // save the member
      tap(member => addMember(member, source)),
      // finally count the members
      count()
    )
    .subscribe(count => {
      runConstraintChecks()
      addMemberCountMessage(<number>count)
    })

  setLoading(false)
}