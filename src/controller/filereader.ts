import { Observable, Observer } from 'rxjs'

export const readFile = (blob: Blob): Observable<string> => Observable.create(
  function (obs: Observer<string>) {
    if (!(blob instanceof Blob)) {
      obs.error(new Error('`blob` must be an instance of File or Blob.'))
      return
    }
    const reader = new FileReader()

    reader.onerror = err => obs.error(err)
    reader.onabort = err => obs.error(err)
    reader.onload = () => obs.next(<string>reader.result)
    reader.onloadend = () => obs.complete()

    return reader.readAsText(blob)
  })


export class RXFileReader {
  blob: Blob

  constructor(file: File) {
    this.blob = file
  }

  public read(): Observable<string> {
    const blob = this.blob
    return Observable.create(
      function (obs: Observer<string>) {
        if (!(blob instanceof Blob)) {
          obs.error(new Error('`blob` must be an instance of File or Blob.'))
          return
        }
        const reader = new FileReader()

        reader.onerror = err => obs.error(err)
        reader.onabort = err => obs.error(err)
        reader.onload = () => obs.next(<string>reader.result)
        reader.onloadend = () => obs.complete()

        return reader.readAsText(blob)
      })
  }
}