import Vue from "vue"
import Vuetify from "vuetify/lib"
import VueRouter from "vue-router"

import App from "@/view/App.vue"
import Main from "@/view/Main.vue"
import MembersTable from "@/view/MembersTable.vue"

import { addSource, addConstraint } from "@/store"
import { sources, constraints } from "@/config"

Vue.use(Vuetify)
Vue.use(VueRouter)
Vue.config.productionTip = false

const vuetify = new Vuetify({})

const routes = [
  { path: '/members/:sourceName', name: 'members', component: MembersTable, props: true, },
  { path: '/', component: Main }
]
const router = new VueRouter({
  routes // short for `routes: routes`
})

// load initial config data
function loadConfig() {
  for (var source of sources) {
    addSource(source)
  }
  for (var constraint of constraints) {
    addConstraint(constraint)
  }
}
loadConfig()


new Vue({
  vuetify,
  router,
  render: h => h(App)
}).$mount("#app")
