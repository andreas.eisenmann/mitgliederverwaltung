import moment from 'moment'

const defaultDateFormat = "DD.MM.YYYY"
const yearFormat = "DD.MM.YYYY"

export function parseDate(input: string, format?: string): Date {
  if (format == null) {
    format = defaultDateFormat
  }
  var mom: moment.Moment = moment(input, format)
  if (!mom.isValid()) {
    mom = moment("31.12.9999", format)
  }
  return mom.toDate()
}

export function dateToString(input: Date, format?: string): string {
  if (format == null) {
    format = defaultDateFormat
  }
  return moment(input).format(format)
}
