var path = require("path");

module.exports = {
  devServer: {
    public: "localhost:8080"
  },
  configureWebpack: {
    devtool: "source-map",
    resolve: {
      modules: [path.resolve("./node_modules"), path.resolve("./src")]
    }
  },
  transpileDependencies: ["vuetify"],
  runtimeCompiler: true
};
